/**
 *
 */
package org.ytrainingmodulebackoffice;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.ytrainingmodulebackoffice.model.ECENTANotificationModel;
import org.ytrainingmodulebackoffice.model.FilterNotificationsCronJobModel;


/**
 * @author janis
 *
 */
public class FilterNotificationsJobPerformable extends AbstractJobPerformable<FilterNotificationsCronJobModel>
{
	private static final Logger LOG = Logger.getLogger(FilterNotificationsJobPerformable.class.getName());

	//Returns ECENTANotificationModel objects that are older than one year
	public List<ECENTANotificationModel> getYearOldNotifications()
	{
		final Calendar cal = Calendar.getInstance();
		cal.add(Calendar.YEAR, -1);
		final Date yearAgo = cal.getTime();

		final StringBuilder query = new StringBuilder("SELECT {pk},{read} FROM {ECENTANotification as e} WHERE {e.date}<?yearAgo");
		final Map<String, Object> params = new HashMap<String, Object>();
		params.put("yearAgo", yearAgo);

		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(query.toString());
		fsq.addQueryParameters(params);
		fsq.setResultClassList(Collections.singletonList(ECENTANotificationModel.class));
		final SearchResult<ECENTANotificationModel> searchResult = flexibleSearchService.search(fsq);
		//searchResult.getResult()
		return searchResult.getResult();
	}

	public void updateNotifications(final List<ECENTANotificationModel> notificationList)
	{
		for (final ECENTANotificationModel a : notificationList)
		{
			if (!a.getRead())
			{
				a.setRead(true);
				a.setDeleted(true);
				modelService.save(a);
			}
		}
	}

	@Override
	public PerformResult perform(final FilterNotificationsCronJobModel cronJobModel)
	{

		final List<ECENTANotificationModel> yearOldNotifications = this.getYearOldNotifications();
		this.updateNotifications(yearOldNotifications);

		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}
}