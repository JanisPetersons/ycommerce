package org.ytrainingmodulebackoffice;

import de.hybris.platform.util.CSVCellDecorator;

import java.util.Map;

/**
 * @author janis
 *
 */
public class MessageDecorator implements CSVCellDecorator
{
	public String decorate(final int position, final Map<Integer, String> srcLine)
   {
		final String csvCell = srcLine.get(Integer.valueOf(position));
		return csvCell + " Sample data line.";
   }
}
