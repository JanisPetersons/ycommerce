/**
 *
 */
package org.ytrainingmodulebackoffice;

import de.hybris.platform.impex.jalo.translators.AbstractValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloInvalidParameterException;
import de.hybris.platform.jalo.security.JaloSecurityException;

import org.apache.commons.lang.StringUtils;


/**
 * @author janis
 *
 */
public class TitleTranslator extends AbstractValueTranslator
{
	@Override
	public Object importValue(final String valueExpr, final Item toItem) throws JaloInvalidParameterException
	{
		String result = "", message = "";
		if (!StringUtils.isBlank(valueExpr))
		{
			result = valueExpr;
		}
		else
		{
			try
			{
				if (toItem == null)
				{
					setError();
				}
				else
				{
					message = toItem.getAttribute("message").toString();
					final int len = message.length();
					if (len < 100)
					{
						result = message;
					}
					else
					{
						result = message.substring(0, 99);
					}


				}
			}
			catch (final JaloSecurityException e)
			{
				// XXX Auto-generated catch block
				e.printStackTrace();
			}

		}
		return result;
	}

	@Override
	public String exportValue(final Object value) throws JaloInvalidParameterException
	{
		return value == null ? "" : value.toString();
	}

}
